#include "mockwidget.h"

#include <QHBoxLayout>

namespace Klair::Tiling {

MockWidget::MockWidget(QWidget *parent)
    : QWidget { parent }
    , m_root { new QWidget(this) }
    , m_hSplit { nullptr }
    , m_vSplit { nullptr }
    , m_close { nullptr }
{
    draw();
    style();
    connect();
}

void MockWidget::draw()
{
    auto *layout = new QHBoxLayout { m_root };
    m_root->setLayout(layout);
    layout->addWidget((m_hSplit = new QPushButton { "Split H", m_root }));
    layout->addWidget((m_vSplit = new QPushButton { "Split V", m_root }));
    layout->addSpacing(m_spacing);
    layout->addWidget((m_close = new QPushButton { "Close", m_root }));
}

void MockWidget::style()
{
    setObjectName("MockWidget");
    setStyleSheet(R"(
        #MockWidget {
            border: 1px solid yellow;
        }
    )");
}

void MockWidget::connect()
{
    QObject::connect(this->m_vSplit, &QPushButton::pressed, this, [this]() { emit split(SplitDirection::vertical); });
    QObject::connect(this->m_hSplit, &QPushButton::pressed, this, [this]() { emit split(SplitDirection::horizontal); });
    QObject::connect(this->m_close, &QPushButton::pressed, this, [this]() { closing(); });
}

}