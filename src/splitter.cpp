#include "splitter.h"
#include "tiling.h"

#include <QDebug>
#include <iostream>

namespace {
auto counter = 0U;

Klair::Tiling::MainWindow *climbUp(Klair::Tiling::Splitter *splitter)
{
    QWidget *widget = splitter;
    while (widget && !qobject_cast<Klair::Tiling::MainWindow *>(widget)) {
        widget = widget->parentWidget();
    }
    return qobject_cast<Klair::Tiling::MainWindow *>(widget);
}

void printTree(Klair::Tiling::MainWindow *window)
{
    const auto str = QString('-').repeated(20).toStdString();
    std::cout << str << std::endl;
    window->m_root->print();
    std::cout << str << std::endl;
}
}

namespace {
auto toQtDirection(Klair::Tiling::SplitDirection direction)
{
    switch (direction) {
    case Klair::Tiling::SplitDirection::horizontal:
        return Qt::Orientation::Horizontal;
    case Klair::Tiling::SplitDirection::vertical:
        return Qt::Orientation::Vertical;
    }
}
}

namespace Klair::Tiling {
Splitter::Splitter(QWidget *parent)
    : QSplitter(parent)
    , m_first { new Console { this } }
    , m_second { nullptr }
    , m_index { counter++ }
{
    construct();
}

Splitter::Splitter(QWidget *parent, Console *first)
    : QSplitter(parent)
    , m_first { first }
    , m_second { nullptr }
    , m_index { counter++ }
{
    construct();
}

void Splitter::construct()
{
    setOpaqueResize(false);
    addWidget(m_first);
    reconnect();
}

void Splitter::reconnect()
{
    clearConnections();
    for (size_t i = 0; i < count(); ++i) {
        auto item = widget(i);
        if (auto *console = qobject_cast<Console *>(item); console) {
            m_connections.emplace_back(connect(console, &Console::split, [this](Console *console, SplitDirection direction) {
                std::cout << "Splitting console" << std::endl;
                auto root = climbUp(this);
                enactSplit(direction);
                printTree(root);
            }));

            m_connections.emplace_back(connect(console, &Console::nuke, [this](Console *console) {
                std::cout << "Closing console" << std::endl;
                auto root = climbUp(this);
                nukeConsole(console);
                printTree(root);
            }));
        } else if (auto *splitter = qobject_cast<Splitter *>(item); splitter) {
            splitter->reconnect();

            m_connections.emplace_back(connect(splitter, &Splitter::nuke, [this, splitter]() {
                std::cout << "Closing splitter" << std::endl;
                auto root = climbUp(this);
                nukeSplitter(splitter);
                printTree(root);
            }));
        } else {
            throw std::runtime_error { "Splitter ended up with unknown children" };
        }
    }
}

void Splitter::nukeSplitter(Splitter *splitter)
{
    qDebug() << "nuking splitter " << splitter;
    auto firstSplitter = qobject_cast<Splitter *>(m_first);
    auto secondSplitter = qobject_cast<Splitter *>(m_second);
    if (firstSplitter && secondSplitter) {
        auto toSave = splitter == firstSplitter ? secondSplitter : firstSplitter;
        const auto savedSize = toSave->sizes();
        qDebug() << widget(0) << ", " << widget(1);
        qDebug() << "with";
        qDebug() << toSave->m_first << ", " << toSave->m_second;
        replaceWidget(0, toSave->m_first);
        replaceWidget(1, toSave->m_second);
        setOrientation(toSave->orientation());
        setSizes(savedSize);

        auto [first, second] = std::pair { toSave->m_first, toSave->m_second };
        firstSplitter->clearConnections();
        secondSplitter->clearConnections();

        m_first->deleteLater();
        m_second->deleteLater();

        m_first = first;
        m_second = second;
    } else if (secondSplitter) {
        qDebug() << "deleting splitter " << splitter;
        m_second->deleteLater();
        m_second = nullptr;
    } else if (firstSplitter) {
        emit nuke();
    }
    reconnect();
}

void Splitter::nukeConsole(Console *console)
{
    assert(m_first == console);
    if (m_second) {
        auto splitter = qobject_cast<Splitter *>(m_second);
        splitter->clearConnections();
        const auto sizes = splitter->sizes();

        auto [first, second] = std::pair { splitter->m_first, splitter->m_second };
        replaceWidget(0, first);
        replaceWidget(1, second);
        setOrientation(splitter->orientation());
        setSizes(sizes);

        m_first->deleteLater();
        m_second->deleteLater();
        m_first = first;
        m_second = second;
        reconnect();
    } else {
        emit nuke();
    }
}

void Splitter::print(size_t tabs)
{
    auto spaces = QString(' ').repeated(2 * tabs).toStdString();
    auto castAndCall = [tabs, spaces](QWidget *widget) {
        if (auto splitter = qobject_cast<Splitter *>(widget); splitter) {
            std::cout << spaces << "└ " << (char)('A' + splitter->m_index) << std::endl;
            splitter->print(tabs + 1);
        } else if (auto console = qobject_cast<Console *>(widget); console) {
            std::cout << spaces << "└ " << console->m_index << std::endl;
        }
    };
    if (!m_index)
        std::cout << spaces << "+ " << (char)('A' + m_index) << std::endl;
    castAndCall(m_first);
    castAndCall(m_second);
}

void Splitter::enactSplit(SplitDirection direction)
{
    if (m_second) {
        moveFirst(direction);
    } else {
        createSecond(direction);
    }
    reconnect();
}

void Splitter::createSecond(SplitDirection direction)
{
    const auto dim = sizes().front();
    setOrientation(toQtDirection(direction));
    m_second = new Splitter(this);
    setSizes({ dim / 2, dim / 2 });
}

void Splitter::moveFirst(SplitDirection direction)
{
    auto firstConsole = qobject_cast<Console *>(m_first);
    const auto dims = sizes();
    auto splitter = new Splitter(nullptr, firstConsole);
    splitter->enactSplit(direction);
    insertWidget(0, splitter);
    splitter->setSizes({ dims.front() / 2, dims.front() / 2 });
    setSizes(dims);
    m_first = splitter;
}

void Splitter::buttonsEnabled(bool enabled)
{
    auto console = qobject_cast<Console *>(m_first);
    assert(console);
    console->buttonsEnabled(enabled);
}

void Splitter::clearConnections()
{
    std::for_each(m_connections.begin(), m_connections.end(), [](decltype(m_connections)::value_type &connection) {
        disconnect(connection);
    });
    m_connections.clear();
}
}