#include "tiling.h"

#include <QStyle>

#include <iostream>

namespace Klair::Tiling {

MainWindow::MainWindow()
    : m_root(new Splitter { this })
{
    setCentralWidget(m_root);
    // enableQuakeMode();
}

void MainWindow::enableQuakeMode()
{
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignHCenter, { 1920, 400 }, { 0, 0, 1920, 1080 }));
}

}