#include "tiling.h"

#include <QApplication>

int main(int argc, char **argv)
{
    QApplication application { argc, argv };

    Klair::Tiling::MainWindow window;
    window.show();

    QApplication::exec();
}