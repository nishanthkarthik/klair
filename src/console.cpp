#include "console.h"

#include <QDebug>

namespace {
static auto consoleCount = 0U;
}

namespace Klair::Tiling {

Console::Console(QWidget *parent)
    : QWidget(parent)
    , m_layout { new QVBoxLayout { this } }
    , m_tools { new QHBoxLayout { m_layout->widget() } }
    , m_index { consoleCount++ }
    , m_label { new QLabel { ("Terminal " + std::to_string(m_index)).c_str(), this } }
    , m_terminal(Terminal::makeTerminal(this))
    , m_splitHorizontal { new QPushButton { "|", this } }
    , m_splitVertical { new QPushButton { "⸺", this } }
    , m_close { new QPushButton { "⨉", this } }
{
    {
        m_tools->addWidget(m_label);
        m_tools->addWidget(m_splitVertical);
        m_tools->addWidget(m_splitHorizontal);
        m_tools->addWidget(m_close);
    }
    {
        m_splitHorizontal->setMaximumWidth(40);
        m_splitVertical->setMaximumWidth(40);
        m_close->setMaximumWidth(40);
    }
    setLayout(m_layout);
    m_layout->addLayout(m_tools);
    m_layout->addWidget(m_terminal.readOnlyPart->widget());

    connectSignals();
}

void Console::connectSignals()
{
    connect(m_splitHorizontal, &QPushButton::clicked, this, [this] {
        emit split(this, SplitDirection::horizontal);
    });

    connect(m_splitVertical, &QPushButton::clicked, this, [this] {
        emit split(this, SplitDirection::vertical);
    });

    connect(m_close, &QPushButton::clicked, this, &Console::nuking);
}

void Console::nuking()
{
    emit nuke(this);
}

void Console::buttonsEnabled(bool enabled)
{
    m_splitVertical->setEnabled(enabled);
    m_splitHorizontal->setEnabled(enabled);
}
}