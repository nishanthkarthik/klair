#pragma once

#include <QObject>
#include <QPushButton>
#include <QWidget>

#include "console.h"

namespace Klair::Tiling {

class MockWidget : public QWidget {
    Q_OBJECT
public:
    explicit MockWidget(QWidget *parent);

signals:
    void split(SplitDirection direction);
    void closing();

private:
    void draw();
    void style();
    void connect();

    QWidget *m_root;
    QPushButton *m_hSplit;
    QPushButton *m_vSplit;
    QPushButton *m_close;

    static constexpr auto m_spacing = 40U;
};

}