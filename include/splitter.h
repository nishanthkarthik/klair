#pragma once

#include "console.h"

#include <QSplitter>

#include <variant>

namespace Klair::Tiling {
class Splitter : public QSplitter {
    Q_OBJECT
public:
    Splitter(QWidget *parent);
    Splitter(QWidget *parent, Console *first);
    void enactSplit(SplitDirection direction);
    void moveFirst(SplitDirection direction);
    void createSecond(SplitDirection direction);
    void print(size_t tabs = 0);

signals:
    void nuke();

private:
    void construct();
    void reconnect();
    void clearConnections();
    void nukeConsole(Console *console);
    void nukeSplitter(Splitter *splitter);
    void buttonsEnabled(bool enabled);

    QWidget *m_first;
    QWidget *m_second;

    std::vector<QMetaObject::Connection> m_connections;
    size_t m_index;
};
}