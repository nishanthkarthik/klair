#pragma once

#include <QLabel>
#include <QObject>
#include <QPushButton>
#include <QVBoxLayout>
#include <QWidget>

#include <KParts/ReadOnlyPart>
#include <KService>
#include <kde_terminal_interface.h>

#include <exception>

namespace {
constexpr auto KonsolePartName = "konsolepart";
}

namespace Klair::Tiling {
struct Terminal {
    static Terminal makeTerminal(QObject *parent)
    {
        KService::Ptr service = KService::serviceByDesktopName(KonsolePartName);
        if (!service)
            throw std::runtime_error { "Can not find konsolepart service" };

        const auto part = service->createInstance<KParts::ReadOnlyPart>(parent);
        if (!part)
            throw std::runtime_error { "Can not create an instance of konsolepart" };

        const auto interface = qobject_cast<TerminalInterface *>(part);
        return Terminal { part, interface };
    }

    KParts::ReadOnlyPart *readOnlyPart;
    TerminalInterface *terminalInterface;
};

enum class SplitDirection {
    horizontal,
    vertical
};

class Console : public QWidget {
    Q_OBJECT
public:
    explicit Console(QWidget *parent);
    void buttonsEnabled(bool enabled);

    size_t m_index;

signals:
    void split(Console *console, SplitDirection direction);
    void nuke(Console *console);

private:
    void connectSignals();
    void nuking();

    QVBoxLayout *m_layout;
    QHBoxLayout *m_tools;
    QLabel *m_label;
    Terminal m_terminal;

    QPushButton *m_splitHorizontal;
    QPushButton *m_splitVertical;
    QPushButton *m_close;
};
}