#pragma once

#include "console.h"
#include "mockwidget.h"
#include "splitter.h"

#include <QBoxLayout>
#include <QMainWindow>
#include <QSplitter>

#include <variant>

namespace Klair::Tiling {

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    explicit MainWindow();
    Splitter *m_root;

private:
    void enableQuakeMode();
};

}